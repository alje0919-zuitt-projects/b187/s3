<?php require_once "./code.php" ?>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP SC Activity</title>
</head>
<body>
	<h1>Person</h1>
	<p><?= $person->printName(); ?></p>

	<h1>Developer</h1>
	<p><?= $developer->printName(); ?></p>

	<h1>Engineer</h1>
	<p><?= $engineer->printName(); ?></p>

</body>
</html>