<?php

// [] Objects as Classes
class Person {
	public $firstName;
	public $middleName;
	public $lastName;


	// constructor
		// used during the creationg of an object to provide the initial values of each property.

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}
	// [] Methods -- like print all
	public function printName(){
		return "Your full name is $this->firstName $this->lastName.";
	}
}

// [] Inheritance and Polymorphism

class Developer extends Person {
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
}

class Engineer extends Person {
	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}




$person = new Person('Senku', 'MN', 'Ishigami');
$developer = new Developer('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');
